---
description: This is an introduction about what the holoSphere Operating System is
---

# The holoVerse Journey

We will go through a journey within the holoVerse to show you how with the help of an holoMap we can discover what is out there and travel along our path and orient ourselve on the map \( a journey expends oneself only if you can go back to the spot you visited, recall the experience : shortcut to what you have learn ... as you build bridged and portal on your own map and mind \)

## Getting Super Powers

Becoming a super hero is a fairly straight forward process:

![The holosphere is the map of you journey](.gitbook/assets/holosphere.png)

The holoKit and holoBoard are the tools to you to navigate without feeling  lost, that's your compass : 

```
$ give me super-powers
```

{% hint style="info" %}
 Super-powers are granted randomly so please submit an issue if you're not happy with yours.
{% endhint %}

![your compass !](.gitbook/assets/holoring-compass.png)

Once you're strong enough, save the world:

![](.gitbook/assets/krystal.png)

{% code title="hello.sh" %}
```bash
# Ain't no code for that yet, sorry
echo 'You got to trust me on this, I saved the world'
```
{% endcode %}



